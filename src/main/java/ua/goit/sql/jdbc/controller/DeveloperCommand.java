package ua.goit.sql.jdbc.controller;

import ua.goit.sql.jdbc.dao.ConnectDao;
import ua.goit.sql.jdbc.dao.DeveloperDao;
import ua.goit.sql.jdbc.factory.DeveloperFactory;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

public class DeveloperCommand implements Command{
    private final String DEVELOPER = "Developer";
    private final String DEVELOPERS = "Developers";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        DeveloperFactory developerFactory = new DeveloperFactory();
        DeveloperDao developerDao = new DeveloperDao();

        int id;
        String name;
        String surname;
        int salary;

        ConsoleHelper.writeMessage(DEVELOPERS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("\nCreating a new "+ DEVELOPER +":\nEnter id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter surname:\n");
                surname = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter "  + DEVELOPER + " salary:\n");
                salary = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\n" + DEVELOPER + " added!\n");
                developerFactory.createDeveloper(id, name, surname, salary);
                developerDao.showDeveloper(id);
                ConnectDao.statement.close();
                break;
            case 2: //works
                developerDao.showAllDevelopers();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                id = ConsoleHelper.readInt();
                developerDao.showDeveloper(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("1 - update all fields| 2 - update name | 3 - update surname | 4 update salary");
                int choice = ConsoleHelper.readInt();
                switch (choice){
                    case 1:
                        ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                        id = ConsoleHelper.readInt();
                        ConsoleHelper.writeMessage("\nEnter new " + DEVELOPER + " name:\n");
                        name = ConsoleHelper.readString();
                        ConsoleHelper.writeMessage("\nEnter new " + DEVELOPER + " surname:\n");
                        surname = ConsoleHelper.readString();
                        ConsoleHelper.writeMessage("Enter " + DEVELOPER + " salary:\n");
                        salary = ConsoleHelper.readInt();
                        developerDao.updateAllDeveloper(id, name, surname, salary);
                        ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully updated!\n");
                        break;
                    case 2:
                        ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                        id = ConsoleHelper.readInt();
                        ConsoleHelper.writeMessage("\nEnter new " + DEVELOPER + " name:\n");
                        name = ConsoleHelper.readString();
                        developerDao.updateDeveloperName(id, name);
                        ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully updated!\n");
                        break;
                    case 3:
                        ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                        id = ConsoleHelper.readInt();
                        ConsoleHelper.writeMessage("\nEnter new " + DEVELOPER + " Surname:\n");
                        surname = ConsoleHelper.readString();
                        developerDao.updateDeveloperSurname(id, surname);
                        ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully updated!\n");
                        break;
                    case 4:
                        ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                        id = ConsoleHelper.readInt();
                        ConsoleHelper.writeMessage("\nEnter new " + DEVELOPER + " salary:\n");
                        salary = ConsoleHelper.readInt();
                        developerDao.updateDeveloperSalary(id, salary);
                        ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully updated!\n");
                        break;
                }

                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id for removal:\n");
                id = ConsoleHelper.readInt();
                developerDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
