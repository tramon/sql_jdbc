package ua.goit.sql.jdbc.controller;

import ua.goit.sql.jdbc.dao.ConnectDao;
import ua.goit.sql.jdbc.dao.ProjectDao;
import ua.goit.sql.jdbc.factory.ProjectFactory;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

public class ProjectCommand implements Command{
    private final String PROJECT = "Project";
    private final String PROJECTS = "Projects";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        ProjectFactory projectFactory = new ProjectFactory();
        ProjectDao projectDao = new ProjectDao();

        int id;
        String name;
        int cost;

        ConsoleHelper.writeMessage(PROJECTS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("\nCreating a new "+ PROJECT +":\nEnter id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter "  + PROJECT + " cost:\n");
                cost = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\n" + PROJECT + " added!\n");
                projectFactory.createProject(id, name, cost);
                projectDao.showProject(id);
                ConnectDao.statement.close();
                break;
            case 2:
                projectDao.showAllProjects();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + PROJECT + " id:\n");
                id = ConsoleHelper.readInt();
                projectDao.showProject(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + PROJECT + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\nEnter new " + PROJECT + " name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter " + PROJECT + " cost:\n");
                cost = ConsoleHelper.readInt();
                projectDao.updateElement(id, name);
                ConsoleHelper.writeMessage("\n" + PROJECT + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + PROJECT + " id for removal:\n");
                id = ConsoleHelper.readInt();
                projectDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + PROJECT + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
