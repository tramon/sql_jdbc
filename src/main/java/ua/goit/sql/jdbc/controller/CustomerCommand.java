package ua.goit.sql.jdbc.controller;


import ua.goit.sql.jdbc.dao.ConnectDao;
import ua.goit.sql.jdbc.dao.CustomerDao;
import ua.goit.sql.jdbc.factory.CustomerFactory;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;


/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerCommand implements Command {
    private final String CUSTOMER = "Customer";
    private final String CUSTOMERS = "Customers";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        CustomerFactory customerFactory = new CustomerFactory();
        CustomerDao customerDao = new CustomerDao();
        int id;
        String name;
        boolean isVIPClient;

        ConsoleHelper.writeMessage(CUSTOMERS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("\nCreating a new "+ CUSTOMER +":\nEnter id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Is " + CUSTOMER + "customer a VIP client? answer 'Y' or 'N':\n");
                isVIPClient = ConsoleHelper.readBoolean();
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " added!\n");
                customerFactory.createCustomer(id, name);
                customerDao.showCustomer(id);
                ConnectDao.statement.close();
                break;
            case 2:
                customerDao.showAllCustomers();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id:\n");
                id = ConsoleHelper.readInt();
                customerDao.showCustomer(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\nEnter new " + CUSTOMER + " name:\n");
                name = ConsoleHelper.readString();
                customerDao.updateElement(id, name);
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id for removal:\n");
                id = ConsoleHelper.readInt();
                customerDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
