package ua.goit.sql.jdbc.dao;

import ua.goit.sql.jdbc.model.Company;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class CompanyDao {

    private List<Company> listCompanies = null;

    protected List<Company> readingAllElements() throws SQLException {
        String sql = "SELECT * FROM companies";
        resultProcessing(sql);
        return listCompanies;
    }

    protected List<Company> readingElement(int id) throws SQLException {
        String sql = "SELECT * FROM companies WHERE id = " + id;
        resultProcessing(sql);
        return listCompanies;
    }

    public void updateCompany(int id, String name) throws SQLException {
        String sql = "UPDATE companies SET name = ? WHERE id = ?";
        ConnectDao.updateStringRecord(sql, id, name);
    }

    public void deleteElement(int id) throws SQLException {
        String sql = "DELETE FROM companies WHERE id = ?";
        ConnectDao.deleteRecord(sql, id);
    }

    public void createElement(int id, String name, String address) throws SQLException {
        String sql = "INSERT INTO companies VALUES(?, ?, ?)";
        ConnectDao.createIntStringString(sql, id, name, address );
    }

    public void showAllCompanies() throws SQLException {
        readingAllElements();
        for (Company company : listCompanies){
            ConsoleHelper.writeMessage(company.toString());
        }
    }

    public void showCompany(int id) throws SQLException {
        readingElement(id);
        for (Company company : listCompanies){
            ConsoleHelper.writeMessage(company.toString());
        }
    }

    private void resultProcessing(String sql) throws SQLException {
        ResultSet resultSetCompanies = ConnectDao.selectRecord(sql);
        listCompanies = new ArrayList<>();
        while (resultSetCompanies.next()){
            int id = resultSetCompanies.getInt("id");
            String name = resultSetCompanies.getString("name");
            String address = resultSetCompanies.getString("address");
            listCompanies.add(new Company(id, name, address));
        }
        resultSetCompanies.close();
    }






}
