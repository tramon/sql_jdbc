package ua.goit.sql.jdbc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectDao.class);

    public static final String JDBC_DRIVER = "org.postgresql.Driver";
    public static final String DATABASE_URL = "jdbc:postgresql://195.78.112.104/postgres";
    public static final String USER = "postgres";
    public static final String PASSWORD = "12345";

    public static Connection connection = null;
    public static Statement statement = null;
    public static PreparedStatement preparedStatement = null;

    public static void ConnectDB() throws SQLException, ClassNotFoundException {

        Class.forName(JDBC_DRIVER);
        LOGGER.info("Trying to connect to DataBase: " + JDBC_DRIVER);
        connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
        LOGGER.info("Successfully connected.");
    }

    public static ResultSet selectRecord(String sql) throws SQLException {
        statement = connection.createStatement();
        return statement.executeQuery(sql);
    }

    public static void deleteRecord(String sql, int id) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateIntRecord(String sql, int number, int id) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, number);
        preparedStatement.setInt(2, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateStringRecord(String sql, int id, String name) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, name);
        preparedStatement.setInt(2, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void createIntString(String sql, int id, String str1) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, id);
        preparedStatement.setString(2, str1);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void createIntStringString(String sql, int id, String str1, String str2) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, id);
        preparedStatement.setString(2, str1);
        preparedStatement.setString(3, str2);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void createIntStringInt(String sql, int int1, String str1, int int2) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, int1);
        preparedStatement.setString(2, str1);
        preparedStatement.setInt(3, int2);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateAllDeveloperRecord(String sql, int id, String name, String secondString, int salary ) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, secondString);
        preparedStatement.setInt(3, salary);
        preparedStatement.setInt(4, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void closeConnect() throws SQLException {
        connection.close();
    }
}
