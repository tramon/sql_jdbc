package ua.goit.sql.jdbc.dao;

import ua.goit.sql.jdbc.model.Customer;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerDao {

    private List<Customer> listCustomers = null;

    protected List<Customer> readingAllElements() throws SQLException {
        String sql = "SELECT * FROM customers";
        resultProcessing(sql);
        return listCustomers;
    }

    protected List<Customer> readingElement(int id) throws SQLException {
        String sql = "SELECT * FROM customers WHERE id = " + id;
        resultProcessing(sql);
        return listCustomers;
    }

    public void updateElement(int id, String name) throws SQLException {
        String sql = "UPDATE customers SET name = ?  WHERE id = ?";
        ConnectDao.createIntString(sql, id, name);
    }

    public void deleteElement(int id) throws SQLException {
        String sql = "DELETE FROM customers WHERE id = ?";
        ConnectDao.deleteRecord(sql, id);
    }

    public void createElement(int id, String name) throws SQLException {
        String sql = "INSERT INTO customers VALUES(?, ?)";
        ConnectDao.createIntString(sql, id, name);
    }

    public void showAllCustomers() throws SQLException {
        readingAllElements();
        for (Customer customer : listCustomers){
            ConsoleHelper.writeMessage(customer.toString());
        }
    }

    public void showCustomer(int id) throws SQLException {
        readingElement(id);
        for (Customer customer : listCustomers){
            ConsoleHelper.writeMessage(customer.toString());
        }
    }

    private void resultProcessing(String sql) throws SQLException {
        ResultSet resultSetDevelopers = ConnectDao.selectRecord(sql);
        listCustomers = new ArrayList<>();
        while (resultSetDevelopers.next()){
            int id = resultSetDevelopers.getInt("id");
            String name = resultSetDevelopers.getString("name");
            boolean isVIPClient = resultSetDevelopers.getBoolean("isVIPClient");
            listCustomers.add(new Customer(id, name, isVIPClient));
        }
        resultSetDevelopers.close();
    }


}
