package ua.goit.sql.jdbc.dao;


import ua.goit.sql.jdbc.model.Project;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProjectDao{
    private List<Project> listProjects = null;
    private ResultSet resultSetProjects;

    public List<Project> readingAllElements() throws SQLException {
        String sql = "SELECT * FROM projects";
        resultProcessing(sql);
        return listProjects;
    }

    public List<Project> readingProjectsElements(int id) throws SQLException {
        String sql = "SELECT * FROM projects WHERE id = " + id;
        resultProcessing(sql);
        return listProjects;
    }

    public void deleteElement(int id) throws SQLException {
        String sql = "DELETE FROM projects WHERE id = ?";
        ConnectDao.deleteRecord(sql, id);
    }

    public void updateElement(int id, String name) throws SQLException {
        String sql = "update projects set name = ? WHERE id = ?";
        ConnectDao.updateStringRecord(sql, id, name);
    }

    public void createElement(int id, String name, int cost) throws SQLException {
        String sql = "INSERT INTO projects VALUES(?, ?, ?)";
        ConnectDao.createIntStringInt(sql, id, name, cost);
    }

    public void showAllProjects() throws SQLException {
        readingAllElements();
        for (Project project : listProjects){
            ConsoleHelper.writeMessage(project.toString());
        }
    }

    public void showProject(int id) throws SQLException {
        readingProjectsElements(id);
        for (Project project : listProjects){
            ConsoleHelper.writeMessage(project.toString());
        }
    }

    private void resultProcessing(String sql) throws SQLException {
        resultSetProjects = ConnectDao.selectRecord(sql);
        listProjects = new ArrayList<>();

        while (resultSetProjects.next()){
            int id = resultSetProjects.getInt("id");
            String name = resultSetProjects.getString("name");
            int cost = resultSetProjects.getInt("cost");
            Project project = new Project(id, name, cost);
            listProjects.add(project);
        }
        resultSetProjects.close();
    }
}
