package ua.goit.sql.jdbc.dao;

import ua.goit.sql.jdbc.model.Developer;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeveloperDao{
    private List<Developer> listDevelopers = null;

    protected List<Developer> readingAllElements() throws SQLException {
        String sql = "SELECT * FROM developers";
        resultProcessing(sql);
        return listDevelopers;
    }

    protected List<Developer> readingElement(int id) throws SQLException {
        String sql = "SELECT * FROM developers WHERE id = " + id;
        resultProcessing(sql);
        return listDevelopers;
    }

    public void updateAllDeveloper(int id, String name, String surname, int salary) throws SQLException {
        String sql = "UPDATE developers SET name = ?, surname = ?, salary = ? WHERE id =?";
        ConnectDao.updateAllDeveloperRecord(sql, id, name, surname, salary);
    }

    public void updateDeveloperName(int id, String name) throws SQLException {
        String sql = "UPDATE developers SET name = ? WHERE id =?";
        ConnectDao.updateStringRecord(sql, id, name);
    }

    public void updateDeveloperSurname(int id, String surname) throws SQLException {
        String sql = "UPDATE developers SET surname = ? WHERE id =?";
        ConnectDao.updateStringRecord(sql, id, surname);
    }

    public void updateDeveloperSalary(int id, int salary) throws SQLException {
        String sql = "UPDATE developers SET salary = ? WHERE id =?";
        ConnectDao.updateIntRecord(sql, id, salary);
    }

    public void deleteElement(int id) throws SQLException {
        String sql = "DELETE FROM developers WHERE id = ?";
        ConnectDao.deleteRecord(sql, id);
    }

    public void createElement(int id, String name, String surname, int salary ) throws SQLException {
        String sql = "INSERT INTO developers VALUES(?, ?, ?, ?)";
        ConnectDao.updateAllDeveloperRecord(sql, id, name, surname, salary); //TODO Surname lost - need to implement in ConnectDao.
    }

    public void showAllDevelopers() throws SQLException {
        readingAllElements();
        for (Developer developer :listDevelopers){
            ConsoleHelper.writeMessage(developer.toString());
        }
    }

    public void showDeveloper(int id) throws SQLException {
        readingElement(id);
        for (Developer developer :listDevelopers){
            ConsoleHelper.writeMessage(developer.toString());
        }
    }

    private void resultProcessing(String sql) throws SQLException {
        ResultSet resultSetDevelopers = ConnectDao.selectRecord(sql);
        listDevelopers = new ArrayList<>();
        while (resultSetDevelopers.next()){
            int id = resultSetDevelopers.getInt("id");
            String name = resultSetDevelopers.getString("name");
            String surname = resultSetDevelopers.getString("surname");
            int salary = resultSetDevelopers.getInt("salary");
            listDevelopers.add(new Developer(id, name, surname, salary));
        }
        resultSetDevelopers.close();
    }
}