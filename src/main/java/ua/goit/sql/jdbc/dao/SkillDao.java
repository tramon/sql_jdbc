package ua.goit.sql.jdbc.dao;

import ua.goit.sql.jdbc.model.Skill;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class SkillDao {

    private List<Skill> listSkills = null;

    protected List<Skill> readingAllElements() throws SQLException {
        String sql = "SELECT * FROM skills";
        resultProcessing(sql);
        return listSkills;
    }

    protected List<Skill> readingElement(int id) throws SQLException {
        String sql = "SELECT * FROM skills WHERE id = " + id;
        resultProcessing(sql);
        return listSkills;
    }

    //TODO Needs to be checked
    public void updateElement(int id, int projectId) throws SQLException {
        String sql = "UPDATE skills SET projects.id = ? WHERE id = ?";
        ConnectDao.updateIntRecord(sql, projectId, id);
    }

    public void deleteElement(int id) throws SQLException {
        String sql = "DELETE FROM skills WHERE id = ?";
        ConnectDao.deleteRecord(sql, id);
    }

    public void createElement(int id, String name, String level) throws SQLException {
        String sql = "INSERT INTO skills VALUES(?, ?, ?)";
        ConnectDao.updateStringRecord(sql, id, name);
    }

    public void showAllCompanies() throws SQLException {
        readingAllElements();
        for (Skill skill : listSkills){
            ConsoleHelper.writeMessage(skill.toString());
        }
    }

    public void showCompany(int id) throws SQLException {
        readingElement(id);
        for (Skill skill : listSkills){
            ConsoleHelper.writeMessage(skill.toString());
        }
    }

    private void resultProcessing(String sql) throws SQLException {
        ResultSet resultSkill = ConnectDao.selectRecord(sql);
        listSkills = new ArrayList<>();
        while (resultSkill.next()){
            int id = resultSkill.getInt("id");
            String name = resultSkill.getString("name");
            String address = resultSkill.getString("level");
            listSkills.add(new Skill(id, name, address));
        }
        resultSkill.close();
    }


}
