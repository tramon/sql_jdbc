package ua.goit.sql.jdbc.model;

/**
 * Created by tramo_000 on 30.11.2016.
 */
public class Company {

    private int id;
    private String name;
    private String address;

    public Company() {
    }

    public Company(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "=====| Company |======================================\n" +
                "id: \t\t" + id +  "\n" +
                "Name: \t\t" + name +  "\n" +
                "Address: \t"  + address;
    }
}
