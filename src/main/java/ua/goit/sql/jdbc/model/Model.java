package ua.goit.sql.jdbc.model;

public interface Model {
    public String toString();
}
