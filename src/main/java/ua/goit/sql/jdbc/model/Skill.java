package ua.goit.sql.jdbc.model;

/**
 * Created by tramon on 30.11.2016.
 */
public class Skill {

    private int id;
    private String name;
    private String level;

    public Skill() {
    }

    public Skill(int id, String name, String level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "=====| Skill |======================================\n" +
                "id: \t\t" + id + "\n" +
                "Name: \t\t" + name +  "\n" +
                "Level: \t\t"  + level + "\n";
    }


}
