package ua.goit.sql.jdbc.model;

public class Developer implements Model{
    private int id;
    private String name;
    private String surname;
    private int salary;

    public Developer() {
    }

    public Developer(int id, String name, String surname, int salary) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "=====| Developer |======================================" + "\n" +
                "id: \t\t" + id + "\n" +
                "Name: \t\t" + name + "\n" +
                "Surname: \t" + surname + "\n" +
                "Salary: \t" + salary;
    }
}
