package ua.goit.sql.jdbc.model;


public class Customer {

    private int id;
    private String name;
    private boolean isVIPClient;

    public Customer() {
    }

    public Customer(int id, String name, boolean isVIPClient) {
        this.id = id;
        this.name = name;
        this.isVIPClient = isVIPClient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVIPClient() {
        return isVIPClient;
    }

    public void setVIPClient(boolean VIPClient) {
        isVIPClient = VIPClient;
    }

    @Override
    public String toString() {
        return "=====| Customer |======================================\n" +
                "id: \t\t\t" + id +  "\n" +
                "Name: \t\t\t" + name +  "\n" +
                "VIP client: \t"  + isVIPClient;
    }
}
