package ua.goit.sql.jdbc.factory;

import ua.goit.sql.jdbc.dao.CompanyDao;
import ua.goit.sql.jdbc.dao.CustomerDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerFactory {

    public void createCustomer (int id, String name) throws SQLException {
        CustomerDao customerDao = new CustomerDao();
        customerDao.createElement(id, name);
    }

}
