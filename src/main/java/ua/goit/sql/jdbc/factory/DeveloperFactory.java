package ua.goit.sql.jdbc.factory;


import ua.goit.sql.jdbc.dao.DeveloperDao;

import java.sql.SQLException;

public class DeveloperFactory{

    public void createDeveloper(int id, String name, String surname, int salary) throws SQLException {
        DeveloperDao developerDao = new DeveloperDao();
        developerDao.createElement(id, name, surname, salary);
    }
}
