package ua.goit.sql.jdbc.factory;

import ua.goit.sql.jdbc.dao.ProjectDao;

import java.sql.SQLException;

public class ProjectFactory {

    public void createProject(int id, String name, int cost) throws SQLException {
        ProjectDao projectDao = new ProjectDao();
        projectDao.createElement(id, name, cost);
    }
}
