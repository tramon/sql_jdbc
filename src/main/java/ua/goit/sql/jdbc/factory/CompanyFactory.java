package ua.goit.sql.jdbc.factory;

import ua.goit.sql.jdbc.dao.CompanyDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class CompanyFactory {

    public void createCompany (int id, String name, String address) throws SQLException {
        CompanyDao companyDao = new CompanyDao();
        companyDao.createElement(id, name, address);
    }

}
