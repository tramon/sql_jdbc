package ua.goit.sql.jdbc.factory;

import ua.goit.sql.jdbc.dao.SkillDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class SkillFactory {

    public void createSkill(int id, String name, String level) throws SQLException {
        SkillDao skillDao = new SkillDao();
        skillDao.createElement(id, name, level);
    }
}
