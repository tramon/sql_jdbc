package ua.goit.sql.jdbc;

import ua.goit.sql.jdbc.controller.*;
import ua.goit.sql.jdbc.dao.ConnectDao;
import ua.goit.sql.jdbc.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

public class ProgramRun {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        ConnectDao.ConnectDB();
        Command command;
        int commandNumber;
        ConsoleHelper.writeMessage("Hello this is the SQL JDBC CRUD App!");

        while (true) {
        ConsoleHelper.writeMessage("\nPlease choose: 1-Companies | 2-Developers | 3-Projects | 4-Customers | 0-Quit App");
            commandNumber = ConsoleHelper.readInt();
            switch (commandNumber) {
                case 0:
                    ConnectDao.closeConnect();
                    return;
                case 1:
                    command = new CompanyCommand();
                    command.execute();
                    break;
                case 2:
                    command = new DeveloperCommand();
                    command.execute();
                    break;
                case 3:
                    command = new ProjectCommand();
                    command.execute();
                    break;
                case 4:
                    command = new CustomerCommand();
                    command.execute();
                    break;
                case 5:
                    command = new SkillCommand();
                    command.execute();
                    break;
                default:
                    break;
            }

        }
    }
}
