package ua.goit.sql.jdbc.view;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString() throws IOException {
        return bufferedReader.readLine();
    }

    public static Boolean readBoolean() throws IOException {
        boolean isVIPClient = false;
        String  inputString = bufferedReader.readLine();
        switch (inputString) {
            case "true":
                isVIPClient = true;
                break;
            case "TRUE":
                isVIPClient = true;
                break;
            case "True":
                isVIPClient = true;
                break;
            case "yes":
                isVIPClient = true;
                break;
            case "YES":
                isVIPClient = true;
                break;
            case "y":
                isVIPClient = true;
                break;
            case "Y":
                isVIPClient = true;
                break;
            default:
                break;
        }
        return isVIPClient;
    }

    public static int readInt() throws IOException {
        int number = 0;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        }catch (NumberFormatException e){
            ConsoleHelper.writeMessage("Incorrect data input. Please enter valid data!");
            readInt();
        }
        return number;
    }
}
