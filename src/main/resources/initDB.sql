CREATE TABLE COMPANIES(
  id integer PRIMARY KEY NOT NULL,
  name varchar(200) NOT NULL,
  address varchar(200)
);

CREATE TABLE DEVELOPERS(
  id integer PRIMARY KEY NOT NULL,
  name varchar(200) NOT NULL,
  surname varchar(200),
  salary integer
);

CREATE TABLE PROJECTS(
  id integer PRIMARY KEY NOT NULL,
  name varchar(200) NOT NULL,
  cost integer
);

CREATE TABLE CUSTOMERS(
  id integer PRIMARY KEY NOT NULL,
  name varchar(200) NOT NULL,
  isVIPClient bool
);

CREATE TABLE SKILLS(
  id integer PRIMARY KEY NOT NULL,
  name varchar(200) NOT NULL,
  level varchar(200)
);

CREATE TABLE CUSTOMER_PROJECTS(
  id integer PRIMARY KEY NOT NULL,
  customerId integer references CUSTOMERS(id) NOT NULL,
  projectId integer references PROJECTS(id) NOT NULL
);

CREATE TABLE DEVELOPER_SKILLS(
  id integer PRIMARY KEY NOT NULL,
  developerId integer references DEVELOPERS(id) NOT NULL,
  skillId integer references SKILLS(id) NOT NULL
);

CREATE TABLE PROJECT_DEVELOPERS(
  id integer PRIMARY KEY NOT NULL,
  projectId integer references PROJECTS(id) NOT NULL, 
  developerId integer references DEVELOPERS(id) NOT NULL
);

CREATE TABLE COMPANY_CUSTOMERS(
  id integer PRIMARY KEY NOT NULL,
  companyId integer references COMPANIES(id) NOT NULL,
  customerId integer references CUSTOMERS(id) NOT NULL
);